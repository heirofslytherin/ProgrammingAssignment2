## Put comments here that give an overall description of what your
## functions do

## makeCacheMatrix
## Purpose : Create a matrix object that has the ability to cache its values and inverse.
makeCacheMatrix <- function(x = matrix()) {
  
  ##Storage for the inverse of the matrix.
  matinv <- NULL
  
  ##Member function that sets the object's data values.
  set <- function(y) {
    x <<- y
    matinv <<- NULL
  }
  
  ##Member function that returns the object's data values.
  get <- function() x
  
  ##Member function that sets the object's matrix inverse.
  setinv <- function(solve) matinv <<- solve
  
  ##Member function that returns the object's matrix inverse.
  getinv <- function() matinv
  
  list(set = set, get = get,
       setinv = setinv,
       getinv = getinv)
}


## cacheSolve
## Purpose : Function that accepts a CacheMatrix object and calculates its inverse.  If its inverse already exists, it draws it from cache.  
##           Either way, return the inverse.
cacheSolve <- function(x, ...) {
        
  
  ##Gets the inverse stored in the CacheMatrix object 'x'.
  matinv <- x$getinv()
  
  ##If there is an inverse (!NULL), then pull that inverse and break out of the function with a return.
  if(!is.null(matinv)) {
    message("Getting cached inverse.")
    return(matinv)
  }
  
  ##If there isn't an inverse, continue on...
  
  ##Grab the object's vector...
  data <- x$get()
  
  ##Use solve to calculate it's inverse...
  matinv <- solve(data, ...)
  
  ##Set the object's inverse value to the calculated value...
  x$setinv(matinv)
  
  ##And print the value of the inverse.
  matinv
}

#Sample output.

#> M <- matrix(sample.int(9, 3*3, TRUE), 3, 3)
#> M
#[,1] [,2] [,3]
#[1,]    3    5    9
#[2,]    7    1    9
#[3,]    6    5    9
#> source('B:/Coursera/R Programming/ProgrammingAssignment2/cachematrix.R')
#> cacmat<-makeCacheMatrix(M)
#> cacheSolve(cacmat)
#[,1]          [,2]       [,3]
#[1,] -0.33333333  3.172066e-17  0.3333333
#[2,] -0.08333333 -2.500000e-01  0.3333333
#[3,]  0.26851852  1.388889e-01 -0.2962963
#> ##For comparison, let's check using just solve.
#  > solve(M)
#[,1]          [,2]       [,3]
#[1,] -0.33333333  3.172066e-17  0.3333333
#[2,] -0.08333333 -2.500000e-01  0.3333333
#[3,]  0.26851852  1.388889e-01 -0.2962963
#> ##Let's see if the cache works...
#  > cacheSolve(cacmat)
#Getting cached inverse.
#[,1]          [,2]       [,3]
#[1,] -0.33333333  3.172066e-17  0.3333333
#[2,] -0.08333333 -2.500000e-01  0.3333333
#[3,]  0.26851852  1.388889e-01 -0.2962963

